package com.example.plantifulapp.Handlers

import android.util.Log
import com.example.plantifulapp.Models.Plant
import com.google.firebase.database.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

object PlantHandler : Observable() {
    private var mValueDataListener: ValueEventListener? = null
    private var mPlantList: ArrayList<Plant>? = ArrayList()
    private var TAG: String = "PlantModel"

    // Return plants:
    fun getPlants(): ArrayList<Plant>?{
        return mPlantList
    }

    // Get database reference
    private fun getDatabaseRef():DatabaseReference {
        return FirebaseDatabase.getInstance().reference.child("users").child("testuser").child("plants")
    }

    init {
        if (mValueDataListener != null) {
            getDatabaseRef()
                ?.removeEventListener(mValueDataListener!!)
        }

        mValueDataListener = null
        Log.i(TAG, "data init line 32")

        mValueDataListener = object: ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                    Log.i(TAG, "data updated line 37")
                    val data: ArrayList<Plant> = ArrayList()
                    if (dataSnapshot != null) {
                        for (snapshot: DataSnapshot in dataSnapshot.children) {
                            try {
                                data.add(Plant(snapshot))
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                        mPlantList = data
                        setChanged()
                        notifyObservers()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                if (databaseError != null) {
                    Log.i(TAG, "line 58 data update canceled, err = ${databaseError.message}")
                }
            }
        }
        getDatabaseRef()
            ?.addValueEventListener(mValueDataListener as ValueEventListener)
    }
}