package com.example.plantifulapp.Handlers

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.plantifulapp.Models.Plant
import com.example.plantifulapp.R


class WindowsillHandler(val items: ArrayList<Plant> ) :
        RecyclerView.Adapter<WindowsillHandler.ViewHolder>() {

        // Create a viewholder
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_plantitem, parent, false)
            return ViewHolder(v)
        }

        // Returns item list size
        override fun getItemCount(): Int {
            return items.size
        }

    // Fills the holders with informations out of the item list
        @SuppressLint("NewApi")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.plantItem.setOnClickListener {
                // Sends message with position in list
                MessageHandler.startMessage()
                MessageHandler.sendMessage(position)
            }
            holder.txtName.text = items[position].name

            var fertility: Int = items[position].fertility.toInt()
            var moisture: Int = items[position].moisture.toInt()
            var light: Int = items[position].light.toInt()


        // Check what emotion flower pot we need:
            if (fertility < 30 || moisture < 30 || light < 30) {
                holder.imgPlantEmotion.setImageResource(R.drawable.ic_sad)

                // check what icon should pop up:
                if (moisture < 30) {
                    holder.imgPlantNeed.setImageResource(R.drawable.ic_moisture)
                }
                else if (light < 30) {
                    holder.imgPlantNeed.setImageResource(R.drawable.ic_light)
                }
                else if (fertility < 30) {
                    holder.imgPlantNeed.setImageResource(R.drawable.ic_fertility)
                }

                holder.imgPlantNeed.visibility = View.VISIBLE;

            }
            else if (fertility < 50 || moisture < 50 || light <50) {
                holder.imgPlantEmotion.setImageResource(R.drawable.ic_meh)
            }
            else {
                holder.imgPlantEmotion.setImageResource(R.drawable.ic_happy)
            }

            when(items[position].species) {
                "aloeplant" -> holder.imgPlant.setImageResource(R.drawable.ic_snakeplant)
                "snakeplant" -> holder.imgPlant.setImageResource(R.drawable.ic_snakeplant)
                "succulent" -> holder.imgPlant.setImageResource(R.drawable.ic_succulent)
                "peacelily" -> holder.imgPlant.setImageResource(R.drawable.ic_peacelily)
                "tulip" -> holder.imgPlant.setImageResource(R.drawable.ic_tulip)
                else -> holder.imgPlant.setImageResource(R.drawable.ic_cactus)
            }
        }

    // Creates the holders for the onbindviewholder
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val txtName = itemView.findViewById<TextView>(R.id.name)
        val imgPlant = itemView.findViewById<ImageView>(R.id.imagePlant)
        val imgPlantEmotion = itemView.findViewById<ImageView>(R.id.imagePlantEmotion)
        val imgPlantNeed = itemView.findViewById<ImageView>(R.id.imagePlantNeed)
        val plantItem = itemView.findViewById<RelativeLayout>(R.id.plantItem)
    }
}


