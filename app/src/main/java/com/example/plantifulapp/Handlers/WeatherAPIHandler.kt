package com.example.plantifulapp.Handlers

import android.os.Handler
import android.os.HandlerThread
import com.example.plantifulapp.Models.Forecast
import com.example.plantifulapp.Models.ForecastJSON
import com.google.gson.Gson
import okhttp3.*
import java.io.IOException

object WeatherAPIHandler : HandlerThread("WeatherAPIHandler") {
    private val TAG: String = "WeatherUIHandler"
    private var handler: Handler? = null

    private var weatherThread: HandlerThread? = null
    private var weatherHandler: Handler? = null

    private val client = OkHttpClient()
    private val forecast = Forecast()
    private var long: String = ""
    private var lat: String = ""

    // Starts thread
    fun startMessage(){
        weatherThread = HandlerThread(TAG, Thread.NORM_PRIORITY)
        weatherThread!!.start()

        weatherHandler = Handler(weatherThread!!.getLooper())
    }

    // Stops thread
    fun stopMessage() {
        weatherThread!!.quitSafely()
    }

    // Sets handler
    fun setHandler(handler: Handler) {
        WeatherAPIHandler.handler = handler
    }

    fun setLocation(lat:String?, long:String?){
        if (lat != null) {
            this.lat = lat
        }

        if (long != null) {
            this.long = long
        }
    }

    fun getForecast(url: String){
        val request = Request.Builder()
            .url(url)
            .build()

        //make a new call
        client.newCall(request).enqueue(object : Callback {
            //If failed
            override fun onFailure(call: Call, e: IOException) {
            }

            //If response
            override fun onResponse(call: Call, response: Response){
                //save json in string
                var json = response.body()?.string()

                //Parse JSON object to kotlin class
                var jsonModel = Gson().fromJson(json, ForecastJSON::class.java)
                forecast.iconWeather    = jsonModel.currently?.icon.toString()
                forecast.summary        = jsonModel.currently?.summary.toString()
                forecast.temperature    = jsonModel.currently?.temperature!!

                forecast.setCelsius()

                sendMessage()
            }
        })
    }

    // Sends message to the target handler (plantInfoActivity)
    fun sendMessage() {
        if (handler == null) return

        handler?.obtainMessage(1,  forecast)?.apply {
            sendToTarget()
        }
    }

}