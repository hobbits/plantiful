package com.example.plantifulapp.Handlers

import android.util.Log
import com.example.plantifulapp.Models.Personality
import com.google.firebase.database.*
import java.util.*
import kotlin.collections.ArrayList

object PersonalityHandler : Observable() {
    private var mValueDataListener: ValueEventListener? = null
    private var mPersonalityList: ArrayList<Personality>? = ArrayList()
    private var TAG: String = "PersonalityModel"

    // Return personalities:
    fun getPersonalities(): ArrayList<Personality>?{
        return mPersonalityList
    }

    // Get database reference
    private fun getDatabaseRef(): DatabaseReference {
        return FirebaseDatabase.getInstance().reference.child("personalities")
    }

    init {
        if (mValueDataListener != null) {
            getDatabaseRef()
                ?.removeEventListener(mValueDataListener!!)
        }

        mValueDataListener = null
        Log.i(TAG, "data init line 31")

        mValueDataListener = object: ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                    Log.i(TAG, "data updated line 36")
                    val data: ArrayList<Personality> = ArrayList()
                    if (dataSnapshot != null) {
                        for (snapshot: DataSnapshot in dataSnapshot.children) {
                            try {
                                data.add(Personality(snapshot))
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                        mPersonalityList = data
                        setChanged()
                        notifyObservers()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                if (databaseError != null) {
                    Log.i(TAG, "line 57 data update canceled, err = ${databaseError.message}")
                }
            }
        }
        getDatabaseRef()
            ?.addValueEventListener(mValueDataListener as ValueEventListener)
    }
}