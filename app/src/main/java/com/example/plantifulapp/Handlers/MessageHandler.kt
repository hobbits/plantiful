package com.example.plantifulapp.Handlers

import android.os.Handler
import android.os.HandlerThread

object MessageHandler : HandlerThread("MessageHandler") {
    private val TAG: String = "MessageHandler"
    private var handler: Handler? = null

    private var messageThread: HandlerThread? = null
    private var messageHandler: Handler? = null

    // Starts thread
    fun startMessage(){
        messageThread = HandlerThread(TAG, Thread.NORM_PRIORITY)
        messageThread!!.start()

        messageHandler = Handler(messageThread!!.getLooper())
    }

    // Stops thread
    fun stopMessage() {
        messageThread!!.quitSafely()
    }

    // Sets handler
    fun setHandler(handler: Handler) {
        MessageHandler.handler = handler
    }

    // Sends message to the target handler (plantInfoActivity)
    fun sendMessage(itemPosition: Int) {
        if (handler == null) return

        handler?.obtainMessage(itemPosition)?.apply {
            sendToTarget()
        }
    }

}