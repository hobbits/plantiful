package com.example.plantifulapp.Models


import com.google.gson.annotations.SerializedName

data class ForecastJSON(
    @SerializedName("currently")
    val currently: Currently?,
    @SerializedName("latitude")
    val latitude: Double?,
    @SerializedName("longitude")
    val longitude: Double?,
    @SerializedName("offset")
    val offset: Int?,
    @SerializedName("timezone")
    val timezone: String?
)