package com.example.plantifulapp.Models

import com.google.firebase.database.DataSnapshot
import java.lang.Exception

class Personality (snapshot: DataSnapshot){

    lateinit var id: String
    lateinit var Random: Array<String>

    init {
        try {
            val data: HashMap<String, Any> = snapshot.value as HashMap<String, Any>

            id =snapshot.key ?: ""
            var randomString = data["random"] as String

            Random = randomString.split(" - ").toTypedArray()
        }
        catch (e: Exception) {
            e.printStackTrace()
        }
    }
}