package com.example.plantifulapp.Models

import kotlin.properties.Delegates


class Forecast {
    var temperature by Delegates.notNull<Double>()
    var iconWeather: String = ""
    var summary: String = ""
    var celcius: Int = 0

    fun setCelsius(){
        celcius = calculateCelsius(temperature)
    }

    private fun calculateCelsius(temperature: Double): Int{
        //Convert Fahrenheit to Celcius
        var result: Double =  (temperature - 32) * 5/9

        //Round double to no decimals
        val number3digits:Double = Math.round(result * 1000.0) / 1000.0
        val number2digits:Double = Math.round(number3digits * 100.0) / 100.0
        val number1digit:Double = Math.round(number2digits * 10.0) / 10.0
        val solution:Double = Math.round(number1digit * 1.0) / 1.0

        return solution.toInt()
    }
}