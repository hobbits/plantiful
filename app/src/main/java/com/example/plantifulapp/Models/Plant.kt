package com.example.plantifulapp.Models

import com.google.firebase.database.DataSnapshot
import java.lang.Exception

class Plant (snapshot: DataSnapshot){
    lateinit var id: String
    lateinit var fertility: String
    lateinit var light: String
    lateinit var moisture: String
    lateinit var name: String
    lateinit var personality: String
    lateinit var species: String
    lateinit var temperature: String

    init {
        try {
            val data: HashMap<String, Any> = snapshot.value as HashMap<String, Any>

            id =snapshot.key ?: ""
            fertility   =   data["fertility"] as String
            light       =   data["light"] as String
            moisture    =   data["moisture"] as String
            name        =   data["name"] as String
            personality =   data["personality"] as String
            species     =   data["species"] as String
            temperature =   data["temperature"] as String
        }
        catch (e: Exception) {
            e.printStackTrace()
        }
    }
}