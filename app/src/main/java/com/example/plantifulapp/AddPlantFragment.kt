package com.example.plantifulapp


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.plantifulapp.Models.Personality
import com.example.plantifulapp.Handlers.PersonalityHandler
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_add_plant.*

class AddPlantFragment : Fragment() {

    private lateinit var database: DatabaseReference
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        database = FirebaseDatabase.getInstance().getReference().child("users").child("testuser").child("plants")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_plant, container, false)
    }

    override fun onStart() {
        setSpinnerAdapter()
        tiEditName.setText(getRandomName())

        btnSync.setOnClickListener() {
            addPlant()
        }

        val stb = AnimationUtils.loadAnimation(this.context, R.anim.scaletobig)
        llPlant.startAnimation(stb)

        super.onStart()
    }

    private fun getRandomName(): String{
        val names: Array<String> = getResources().getStringArray(R.array.arrayNames)
        val min = 0
        val max = (names.size -1)
        val rnd = (min..max).random()

        return names[rnd]
    }

    private fun getRandomPersonality():String{
        val Personalities: ArrayList<Personality>? = PersonalityHandler.getPersonalities()
        val min = 0;
        val max = (Personalities!!.size - 1)
        val rnd = (min..max).random()

        return Personalities[rnd].id
    }

    private fun setSpinnerAdapter(){
        val spinner: Spinner = speciesSpinner
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this.context!!,
            R.array.arraySpecies,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long){
                when(spinner.getSelectedItem().toString()) {
                    "aloeplant" -> imagePlant.setImageResource(R.drawable.ic_snakeplant)
                    "snakeplant" -> imagePlant.setImageResource(R.drawable.ic_snakeplant)
                    "succulent" -> imagePlant.setImageResource(R.drawable.ic_succulent)
                    "peacelily" -> imagePlant.setImageResource(R.drawable.ic_peacelily)
                    "tulip" -> imagePlant.setImageResource(R.drawable.ic_tulip)
                    else -> imagePlant.setImageResource(R.drawable.ic_cactus)
                }
            }

        }
    }

    private fun addPlant(){
        val spinner: Spinner = speciesSpinner

        val fertility = (0..100).random().toString()
        val moisture = (0..100).random().toString()
        val light = (0..100).random().toString()
        val temperature = (15..25).random().toString()
        val id = tiEditName.text.toString().toLowerCase() + fertility + moisture + light + temperature

        val newP = newPlant(
            tiEditName.text.toString(),
            spinner.getSelectedItem().toString(),
            fertility,
            moisture,
            light,
            temperature,
            getRandomPersonality())

        database.child(id).setValue(newP)
        val Toast = Toast.makeText(this.context, tiEditName.text.toString() + " has been added to your windowsill.",  Toast.LENGTH_LONG)
        Toast.show()
    }

    data class newPlant(
        var name :String?           = "",
        var species :String?        = "",
        var fertility :String?      = "",
        var moisture :String?       = "",
        var light :String?          = "",
        var temperature :String?    = "",
        var personality :String?    = ""
    )

}
