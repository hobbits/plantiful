package com.example.plantifulapp.Animations

import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.ProgressBar


class ProgressBarAnimation(private val progressBar: ProgressBar, private val to: Float) : Animation() {

    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
        super.applyTransformation(interpolatedTime, t)
        val from = 0.0f
        val value = from + (to - from) * interpolatedTime
        progressBar.progress = value.toInt()
    }

}