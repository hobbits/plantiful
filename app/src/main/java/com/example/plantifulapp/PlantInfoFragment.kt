package com.example.plantifulapp


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.plantifulapp.Models.*
import kotlinx.android.synthetic.main.fragment_plant_info.*
import java.util.*
import com.example.plantifulapp.Animations.ProgressBarAnimation
import androidx.transition.TransitionManager
import com.example.plantifulapp.Handlers.MessageHandler
import com.example.plantifulapp.Handlers.PersonalityHandler
import com.example.plantifulapp.Handlers.PlantHandler
import com.example.plantifulapp.Handlers.WindowsillHandler


/**
 * A simple [Fragment] subclass.
 */
class PlantInfoFragment : Fragment(), Observer {
     var currentPlant: Plant? = null

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        PlantHandler
        PlantHandler.addObserver(this)

        PersonalityHandler
        PersonalityHandler.addObserver(this)

        // Set the handler to recieve what plant the user clicked on
        MessageHandler.setHandler(handler)

        return inflater.inflate(R.layout.fragment_plant_info, container, false)
    }

    override fun onStart() {

        layoutPlant.setOnClickListener() {
            TransitionManager.beginDelayedTransition(layoutSpeechbubble)
            layoutSpeechbubble.visibility = View.VISIBLE

            val Personality: Personality? = PersonalityHandler.getPersonalities()!!.find{ it.id == currentPlant!!.personality}
            tvPlantPhrase.setText(getRandomPhrase(Personality))

            layoutSpeechbubble.visibility = View.VISIBLE
        }

        val stb = AnimationUtils.loadAnimation(this.context, R.anim.scaletobig)
        layoutPlant.startAnimation(stb)

        super.onStart()
    }

    override fun update(p0: Observable?, p1: Any?) {
        Log.i("Update", "list has been updated")
        for (plant: Plant in PlantHandler.getPlants()!!) {
            Log.i("PlantModel", plant.name)

            // Give the recyvlerview Adapter the right layout + list of plants to show the user
            reclyclerViewWindowsill.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
            reclyclerViewWindowsill.adapter =
                WindowsillHandler(PlantHandler.getPlants()!!)
        }
    }

    override fun onResume() {
        super.onResume()
        PlantHandler.addObserver(this)
        PersonalityHandler.addObserver(this)

        // Give the recyvlerview Adapter the right layout + list of plants to show the user
        reclyclerViewWindowsill.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        reclyclerViewWindowsill.adapter =
            WindowsillHandler(PlantHandler.getPlants()!!)
    }

    override fun onPause() {
        super.onPause()
        PlantHandler.deleteObserver(this)
        PersonalityHandler.deleteObserver(this)
    }

    override fun onStop() {
        super.onStop()
        PlantHandler.deleteObserver(this)
        PersonalityHandler.deleteObserver(this)
    }

    // Create handler to recieve plant selected
    val handler: Handler = object: Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            val itemPosition = msg.what

            // Change the information on the screen & stop the thread
            showPlantInfo(itemPosition)
            MessageHandler.stopMessage()
        }
    }

    fun showPlantInfo(position: Int){
        currentPlant = PlantHandler.getPlants()?.get(position)

        tvPlantName.setText(currentPlant?.name)
        tvPlantSpecies.setText(currentPlant?.species)

        TransitionManager.beginDelayedTransition(layoutSpeechbubble)
        layoutSpeechbubble.visibility = View.INVISIBLE

        changePlantImage()
        changePlantEmotion()
        changeProgressBars()
    }

    @SuppressLint("NewApi")
    fun changePlantEmotion() {

        var fertility: Int = currentPlant?.fertility!!.toInt()
        var moisture: Int = currentPlant?.moisture!!.toInt()
        var light: Int = currentPlant?.light!!.toInt()

        if (fertility < 30 || moisture < 30 || light < 30) {
            imagePlantEmotion.setImageResource(R.drawable.ic_sad)
        }
        else if (fertility < 50 || moisture < 50 || light <50) {
            imagePlantEmotion.setImageResource(R.drawable.ic_meh)
        }
        else {
            imagePlantEmotion.setImageResource(R.drawable.ic_happy)
        }
    }

    fun changePlantImage() {
        when(currentPlant?.species) {
            "aloeplant" -> imagePlant.setImageResource(R.drawable.ic_snakeplant)
            "snakeplant" -> imagePlant.setImageResource(R.drawable.ic_snakeplant)
            "succulent" -> imagePlant.setImageResource(R.drawable.ic_succulent)
            "peacelily" -> imagePlant.setImageResource(R.drawable.ic_peacelily)
            "tulip" -> imagePlant.setImageResource(R.drawable.ic_tulip)
            else -> imagePlant.setImageResource(R.drawable.ic_cactus)
        }
    }

    fun changeProgressBars(){
        val pbMoistureAnimation = ProgressBarAnimation(pbMoisture, currentPlant?.moisture!!.toFloat())
        pbMoistureAnimation.duration = 1000
        pbMoisture.startAnimation(pbMoistureAnimation)

        val pbLightAnimation = ProgressBarAnimation(pbLight, currentPlant?.light!!.toFloat())
        pbLightAnimation.duration = 1000
        pbLight.startAnimation(pbLightAnimation)

        val pbFertilityAnimation = ProgressBarAnimation(pbFertility, currentPlant?.fertility!!.toFloat())
        pbFertilityAnimation.duration = 1000
        pbFertility.startAnimation(pbFertilityAnimation)
    }

    fun getRandomPhrase(personality: Personality?): String{
        val min = 0
        val max = (personality!!.Random.size - 1)
        val rnd = (min..max).random()

        return personality!!.Random[rnd]
    }
}
