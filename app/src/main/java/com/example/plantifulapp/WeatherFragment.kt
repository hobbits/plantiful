package com.example.plantifulapp

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.example.plantifulapp.Handlers.WeatherAPIHandler
import com.example.plantifulapp.Models.Forecast
import kotlinx.android.synthetic.main.fragment_weather.*


private const val ARG_PARAM1 = "long"
private const val ARG_PARAM2 = "lat"

class WeatherFragment : Fragment() {

    private var long: String? = null
    private var lat: String? = null
    private var forecast = Forecast()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        arguments?.let {
            long = it.getString(ARG_PARAM1)
            lat = it.getString(ARG_PARAM2)
        }

        WeatherAPIHandler.setHandler(handler)
        WeatherAPIHandler.startMessage()

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather, container, false)
    }

    override fun onStart() {
        //Get Forecast info
        WeatherAPIHandler.setLocation(lat, long)
        WeatherAPIHandler.getForecast("https://api.darksky.net/forecast/bb64ff408311bd4a81a10e7878a002d2/$lat,$long?exclude=hourly,daily,minutely,alerts,flags")

        //Animation for summary
        val ttb = AnimationUtils.loadAnimation(this.context, R.anim.toptobottom)
        llSummary.startAnimation(ttb)

        super.onStart()
    }

    fun updateUI(){
        tvTemperature.setText(forecast.celcius.toString() + "°C")
        tvSummary.setText(forecast.summary)

        when(forecast.iconWeather) {
            "rain" -> imIcon.setImageResource(R.drawable.ic_rain)
            "clear-day" -> imIcon.setImageResource(R.drawable.ic_clear_day)
            "clear-night" -> imIcon.setImageResource(R.drawable.ic_clear_night)
            "snow" -> imIcon.setImageResource(R.drawable.ic_snow)
            "sleet" -> imIcon.setImageResource(R.drawable.ic_sleet)
            "wind" -> imIcon.setImageResource(R.drawable.ic_wind)
            "fog" -> imIcon.setImageResource(R.drawable.ic_fog)
            "cloudy" -> imIcon.setImageResource(R.drawable.ic_cloudy)
            "partly-cloudy-day" -> imIcon.setImageResource(R.drawable.ic_cloudy)
            "partly-cloudy-night" -> imIcon.setImageResource(R.drawable.ic_clear_night)
            else -> imIcon.setImageResource(R.drawable.ic_clear_day)
        }
    }

    //handler for getting API info for the forecast
    val handler: Handler = object: Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            forecast = msg.obj as Forecast

            // Change the information on the screen & stop the thread
            updateUI()
            WeatherAPIHandler.stopMessage()
        }
    }


    companion object {
        @JvmStatic
        fun newInstance(long: String, lat: String) =
            WeatherFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, long)
                    putString(ARG_PARAM2, lat)
                }
            }
    }
}
