package com.example.plantifulapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class PlantifulActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    var long: String = ""
    var lat: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plantiful)
        
        val intent = intent
        long = intent.getStringExtra("KEY_LONGITUDE")
        lat = intent.getStringExtra("KEY_LATITUDE")

        val navigation: BottomNavigationView = findViewById(R.id.bottom_nav)
        navigation.setOnNavigationItemSelectedListener(this)

        loadFragment(PlantInfoFragment())

    }

    private fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment)
                .commit()

            return true
        }
        return false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null

        when(item.getItemId()) {
            R.id.action_home -> fragment = PlantInfoFragment()
            R.id.action_add -> fragment = AddPlantFragment()
            R.id.action_weather -> fragment = WeatherFragment.newInstance(long, lat)
        }

        return loadFragment(fragment)
    }
}